package ucenfotec.dao;

import ucenfotec.bl.admin.iadmin;
import ucenfotec.bl.usuario.iusuario;

public abstract class DaoFactory {
        public static final int MYSQL = 1;
        public static final int SQLSERVER =2;


        public static DaoFactory DAOfactory(int factory){
            switch (factory){
                case MYSQL:
                    return new MySQLDaoFactory();
                case SQLSERVER:
                    return new SqlServerDaoFactory();
                default:
                    return null;

            }
        }
        public abstract iadmin getAdminDao();
        public abstract iusuario getUsuarioDao();
}
