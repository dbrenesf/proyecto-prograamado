package ucenfotec.dao;

import ucenfotec.bl.admin.AdminDao;
import ucenfotec.bl.admin.iadmin;
import ucenfotec.bl.usuario.UsuarioDao;
import ucenfotec.bl.usuario.iusuario;

public class MySQLDaoFactory extends DaoFactory {

    @Override
    public iadmin getAdminDao(){return new AdminDao();
    }
    @Override
    public iusuario getUsuarioDao(){return new UsuarioDao();

    }
}
