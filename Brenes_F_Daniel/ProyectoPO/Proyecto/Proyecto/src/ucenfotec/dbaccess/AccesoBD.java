package ucenfotec.dbaccess;

import java.sql.*;

public class AccesoBD {

    private Connection conn = null;

    private Statement stmt = null;


    public AccesoBD(String dirver, String StrConexion) throws SQLException,ClassNotFoundException {
        try{
            Class.forName(dirver);
            conn = DriverManager.getConnection(StrConexion);
            stmt = conn.createStatement();
        }
        catch (ClassNotFoundException e){
            //almaceno en el archivo Logs.log
            throw e;
        }

    }

    public AccesoBD(String dirver, String url, String user,String clave)
            throws ClassNotFoundException,SQLException{
        Class.forName(dirver);
        conn = DriverManager.getConnection(url,user,clave);
        stmt = conn.createStatement();
    }


    public void ejecutarQuery(String query) throws SQLException{
        stmt.execute(query);
    }


    public ResultSet ejecutarSQL(String query) throws SQLException{
        ResultSet rs;
        rs = stmt.executeQuery(query);
        return rs;
    }
}