package ucenfotec.dbaccess;


import java.sql.SQLException;


public class Conector {

    private static AccesoBD conectorBD = null;


    public static AccesoBD getConector(String dirver, String connectionStr) throws SQLException, ClassNotFoundException {
        if (conectorBD == null) {
            conectorBD = new AccesoBD(dirver, connectionStr);
        }
        return conectorBD;
    }
}
