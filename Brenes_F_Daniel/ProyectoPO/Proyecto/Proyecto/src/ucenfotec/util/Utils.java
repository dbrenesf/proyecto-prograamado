package ucenfotec.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Utils {

    public static String[] getProperties() throws Exception {

        try (InputStream input = new FileInputStream("src/ucenfotec/ac/cr/util/dbase.props")) {

            Properties prop = new Properties();


            prop.load(input);
            String[] dbConfig = new String[2];


            dbConfig[0] = prop.getProperty("driver");
            dbConfig[1] = prop.getProperty("url");
            return dbConfig;
        }
    }
}