package ucenfotec.bl.admin;

import ucenfotec.bl.usuario.Usuario;
import ucenfotec.dbaccess.Conector;
import ucenfotec.util.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AdminDao implements iadmin{


    @Override
    public void insertar(Admin a) throws ClassNotFoundException, Exception, SQLException {
        String [] dbinfo = Utils.getProperties();
        String query;

        String driver = dbinfo[0];
        String url= dbinfo[1];

        query = "INSERT INTO administrador (usuario,nombre,apellido1,apellido2,contrasenna,correoelectronico) VALUES " +
                "('"+ a.getNombre() + "','" + a.getApellido1() +"','"+a.getApellido2()+"','"+a.getContrasenna()+"','"+a.getContrasenna()+"','"+a.getCorreo()+"')";


        Conector.getConector(driver, url).ejecutarQuery(query);
    }


    @Override
    public ArrayList<Admin> listar() throws ClassNotFoundException, Exception, SQLException {
        ResultSet rs;
        ArrayList<Admin> lista = new ArrayList<>();
        String [] dbinfo = Utils.getProperties();


        String driver = dbinfo[0];
        String url= dbinfo[1];

        String query = "SELECT usuario,nombre,apellido1,apellido2,contrasenna,correoelectronico FROM administrador";
        rs = Conector.getConector(driver, url).ejecutarSQL(query);

        while (rs.next()) {
            Admin tmpA = new Admin();
            tmpA.setUsuario(rs.getString("usuario"));
            tmpA.setNombre(rs.getString("nombre"));
            tmpA.setApellido1(rs.getString("apellido1"));
            tmpA.setApellido2(rs.getString("apellido2"));
            tmpA.setContrasenna(rs.getString("contrasenna"));
            tmpA.setCorreo(rs.getString("correoelectronico"));



            lista.add(tmpA);
        }
        return lista;
    }
}
