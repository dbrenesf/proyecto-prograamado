package ucenfotec.bl.admin;

import ucenfotec.bl.usuario.Usuario;

import java.sql.SQLException;
import java.util.ArrayList;

public interface iadmin {
    void insertar(Admin a) throws ClassNotFoundException,Exception, SQLException;
    ArrayList<Admin> listar()throws ClassNotFoundException,Exception, SQLException;
}