package ucenfotec.bl.usuario;

import java.sql.SQLException;
import java.util.ArrayList;

public interface iusuario {
    void insertar(Usuario u) throws ClassNotFoundException,Exception, SQLException;
    ArrayList<Usuario> listar()throws ClassNotFoundException,Exception, SQLException;
}
