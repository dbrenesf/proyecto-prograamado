package ucenfotec.bl.usuario;

public class Usuario {

    private String nommbre;
    private String apellido1;
    private String apellido2;
    private String edad;
    private String provinvcia;
    private String id;
    private String correo;
    private String usuario;
    private String contrasenna;

    public Usuario(){

    }


    public Usuario(String nommbre, String apellido1, String apellido2, String edad, String provinvcia, String id, String correo, String usuario, String contraseña) {
        this.nommbre = nommbre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.edad = edad;
        this.provinvcia = provinvcia;
        this.id = id;
        this.correo = correo;
        this.usuario = usuario;
        this.contrasenna = contraseña;
    }

    public String getNommbre() {
        return nommbre;
    }

    public void setNommbre(String nommbre) {
        this.nommbre = nommbre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getProvinvcia() {
        return provinvcia;
    }

    public void setProvinvcia(String provinvcia) {
        this.provinvcia = provinvcia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contrasenna;
    }

    public void setContraseña(String contraseña) {
        this.contrasenna = contraseña;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nommbre='" + nommbre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", edad='" + edad + '\'' +
                ", provinvcia='" + provinvcia + '\'' +
                ", id='" + id + '\'' +
                ", correo='" + correo + '\'' +
                ", usuario='" + usuario + '\'' +
                ", contraseña='" + contrasenna + '\'' +
                '}';
    }
}
