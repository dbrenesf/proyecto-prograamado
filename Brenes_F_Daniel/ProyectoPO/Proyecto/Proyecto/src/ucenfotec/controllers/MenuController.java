package ucenfotec.controllers;

import javafx.event.ActionEvent;
import ucenfotec.ToScene;

public class MenuController {
    public void cambiarEscena(ActionEvent event) {
        new ToScene().toScene("/ucenfotec/scenes/main.fxml",event);
    }
}
