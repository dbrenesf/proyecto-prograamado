package ucenfotec.controllers;
import javafx.event.ActionEvent;
import ucenfotec.ToScene;

public class LoginController {

    public void cambiarEscenaMenu(ActionEvent event) {
        new ToScene().toScene("main.fxml",event);
    }

    public void cambiarEscenaSignUp(ActionEvent event){
        new ToScene().toScene("register.fxml",event);
    }
}


