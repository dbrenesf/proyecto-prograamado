package ucenfotec.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import ucenfotec.ToScene;
import javafx.stage.Window;


import java.sql.SQLException;
import java.util.Calendar;


public class Register {
    static dbdaoController gestor = new dbdaoController();
    ObservableList<String> chbprovincialista = FXCollections.observableArrayList("Heredia","San Jose", "Cartago","Alajuela","Puntarenas","Limon","Guanacaste");

    @FXML
    private TextField txnombre;

    @FXML
    private TextField txap1;

    @FXML
    private TextField txap2;

    @FXML
    private TextField txcedula;

    @FXML
    private TextField txcorreo;

    @FXML
    private TextField txuser;

    @FXML
    private TextField txpass;

    @FXML
    private ChoiceBox chbprovincia;

    @FXML
    private DatePicker DCBmain;

    @FXML
    private Button registrarBTN;

    @FXML
    private TextField edad;

    @FXML
    private Button nombrebtn;



    @FXML
    private void initialize(){
        chbprovincia.setItems(chbprovincialista);
        chbprovincia.setValue("Escoga su provincia de residencia");


    }

    @FXML
    public void registrar(ActionEvent event) throws SQLException, ClassNotFoundException {

        Window owner = registrarBTN.getScene().getWindow();



        System.out.println(txnombre.getText());
        System.out.println(txap1.getText());
        System.out.println(txap2.getText());
        System.out.println(txcedula.getText());
        System.out.println(txcorreo.getText());
        System.out.println(txuser.getText());
        System.out.println(txpass.getText());



        String fullName = txnombre.getText();
        String apellido1 = txap1.getText();
        String apellido2 = txap2.getText();
        String edad = getEdad();
        String cedula = txcedula.getText();
        String emailId = txcorreo.getText();
        String usuario = txuser.getText();
        String password = txpass.getText();


        dbdaoController jdbcDao = new dbdaoController();
        jdbcDao.insretarQuery(fullName,apellido1,apellido2,edad,cedula, emailId,usuario, password);

        showAlert(Alert.AlertType.CONFIRMATION, owner, "Registration Successful!",
                "Welcome " + txnombre.getText());
    }

    private static void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }


    @FXML
    private void mostraredad(){

        Calendar now = Calendar.getInstance();
        int anno = now.get(Calendar.YEAR);
        int annoN = (DCBmain.getValue().getYear());
        int edadN = anno - annoN;
        edad.setText(Integer.toString(edadN));


    }

    public String getEdad(){
        Calendar now = Calendar.getInstance();
        int anno = now.get(Calendar.YEAR);
        int annoNacimeinto = (DCBmain.getValue().getYear());
        int edad  = anno - annoNacimeinto;
        return Integer.toString(edad);
    }

    public void cambiarLogin(ActionEvent event) {
        new ToScene().toScene("login.fxml",event);
    }


}
